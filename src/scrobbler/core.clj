(ns scrobbler.core
  (:require [clojure.java.io :as io]
            [scrobbler.id3 :as id3])
  (:gen-class))

(def test-music-folder (io/file "/home/vincent/Downloads/Koufar - Minority Report"))

(id3/parse-all-files-in-directory test-music-folder)

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))
