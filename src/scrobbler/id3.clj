(ns scrobbler.id3
  (:require [clojure.java.io :as io]
            [claudio.id3 :as id3]))

(defn read-tag [f]
  (try
    (id3/read-tag f)
    (catch org.jaudiotagger.audio.exceptions.CannotReadException e
      (println "Whoops!" (.getMessage e)))))

(defn parse-all-files-in-directory [directory]
  (map #(hash-map :file %
                  :id3 (read-tag %))
       (rest (file-seq directory))))
